import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Main extends Application {
    int index = 0, statusIndex = 2;
    ArrayList<Label> label = new ArrayList<>();
    ArrayList<ChoiceBox<String>> country = new ArrayList<>();
    ArrayList<Button> add = new ArrayList<>();
    ArrayList<Button> substract = new ArrayList<>();
    Label labelStatus;
    ChoiceBox<String> status;
    Button submit;
    List<String> allCountries;
    GridPane gridPane;
    Stage primaryStage;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        primaryStage.setTitle("Corona Cases");
        CoronaGraph coronaGraph = new CoronaGraph();

        this.label.add(new Label("Country 1"));

        ChoiceBox<String> country1 = new ChoiceBox<>();
        this.allCountries = coronaGraph.getAllCountries();
        country1.getItems().addAll(this.allCountries);
        this.country.add(country1);

        this.add.add(new Button("+"));

        this.labelStatus = new Label("Status");

        this.status = new ChoiceBox<>();
        Field[] fields = IllnessStatus.class.getFields();
        LinkedList<String> enums = new LinkedList<>();
        for (Field field : fields) {
            enums.add(field.getName());
        }
        this.status.getItems().addAll(enums);

        this.submit = new Button("Show");

        this.gridPane = new GridPane();
        GridPane.setConstraints(this.label.get(0), 0, 0);
        GridPane.setConstraints(country1, 0, 1);
        GridPane.setConstraints(this.add.get(0), 1, 1);
        GridPane.setConstraints(this.labelStatus, 0, 2);
        GridPane.setConstraints(this.status, 0, 3);
        GridPane.setConstraints(this.submit, 0, 4);
        this.gridPane.getChildren().addAll(this.label.get(0), country1, this.add.get(0), this.labelStatus, this.status, this.submit);

        Scene configurationScene = new Scene(this.gridPane, 250, 150);

        primaryStage.setScene(configurationScene);
        primaryStage.centerOnScreen();
        primaryStage.show();

        this.index++;

        this.add.get(0).setOnMouseClicked(e -> setMouseClickedEvent());

        submit.setOnMouseClicked(e -> {
            ArrayList<String> countries = new ArrayList<>();
            for (ChoiceBox<String> countryChoice : this.country) {
                countries.add(countryChoice.getValue());
            }
            countries.trimToSize();
            IllnessStatus statusChoice = IllnessStatus.valueOf(status.getValue());
            primaryStage.setScene(coronaGraph.getSceneFor(countries, statusChoice));
            primaryStage.show();

            coronaGraph.getChangeCountries().setOnAction(ev -> {
                primaryStage.setScene(configurationScene);
                primaryStage.show();
            });
        });
    }

    private void setMouseClickedEvent() {
        this.label.add(new Label("Country " + (this.index + 1)));

        ChoiceBox<String> country2 = new ChoiceBox<>();
        country2.getItems().addAll(this.allCountries);
        this.country.add(country2);

        this.add.add(new Button("+"));
        this.substract.add(new Button(" - "));

        GridPane.setConstraints(this.label.get(index), 0, this.statusIndex++);
        GridPane.setConstraints(this.country.get(index), 0, this.statusIndex);
        GridPane.setConstraints(this.add.get(index), 1, this.statusIndex);
        GridPane.setConstraints(this.substract.get(index - 1), 2, this.statusIndex++);
        GridPane.setConstraints(this.labelStatus, 0, this.statusIndex++);
        GridPane.setConstraints(this.status, 0, this.statusIndex++);
        GridPane.setConstraints(this.submit, 0, this.statusIndex);
        this.gridPane.getChildren().addAll(this.add.get(this.index), this.substract.get(this.index - 1), this.country.get(this.index), this.label.get(this.index));
        this.primaryStage.setMinHeight(this.gridPane.getHeight() + 80);

        this.add.get(this.index).setOnMouseClicked(e -> this.setMouseClickedEvent());

        this.substract.get(this.index - 1).setOnMouseClicked(e -> {
            int index = --this.index;
            this.gridPane.getChildren().removeAll(this.add.remove(index), this.substract.remove(index - 1), this.country.remove(index), this.label.remove(index));
        });

        this.index++;
    }
}
