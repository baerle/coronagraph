import java.util.LinkedList;

public class Country {
    String name;
    LinkedList<Integer> confPersons;
    LinkedList<Integer> deadPersons;
    LinkedList<Integer> recovPersons;

    public Country(String nation) {
        this.name = nation;
        this.confPersons = new LinkedList<>();
        this.deadPersons = new LinkedList<>();
        this.recovPersons = new LinkedList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LinkedList<Integer> getConfPersons() {
        return confPersons;
    }

    public LinkedList<Integer> getDeadPersons() {
        return deadPersons;
    }

    public LinkedList<Integer> getRecovPersons() {
        return recovPersons;
    }
}
